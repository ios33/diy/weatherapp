//
//  CityListData.swift
//  WeatherApp
//
//  Created by HongDT on 11/24/20.
//

import Foundation

final class CityListData {
    static let citiesKey = "Cities"
//    static let defaultCities = [
//        City(name: "1581130"),
//        City(name: "2643743")
//    ]
    
    static func loadCities() -> [Int64] {
        let savedCities = UserDefaults.standard.object(forKey: CityListData.citiesKey)
        if let savedCities = savedCities as? Data {
          let decoder = JSONDecoder()
          return (try? decoder.decode([Int64].self, from: savedCities))
            ?? []//CityListData.defaultCities
        }
        return []//CityListData.defaultCities
    }
    
    var cities = loadCities() {
      didSet {
        persistCities()
      }
    }

    func addCity(city: Int64) {
      let newCity = city
      cities.append(newCity)
    }

    func deleteCity(at offsets: IndexSet) {
      cities.remove(atOffsets: offsets)
    }

    private func persistCities() {
      let encoder = JSONEncoder()
      if let encoded = try? encoder.encode(cities) {
        UserDefaults.standard.set(encoded, forKey: CityListData.citiesKey)
      }
    }
}
