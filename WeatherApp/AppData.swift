//
//  AppData.swift
//  WeatherApp
//
//  Created by HongDT on 11/26/20.
//

import Foundation

struct AppData {
    static let apiKey = "303682ca55e6c9c9d669147c618c7619"
    static let cityList = CityListData()
    static var messageError = ""
}
