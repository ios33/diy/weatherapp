//
//  DetailCityView.swift
//  WeatherApp
//
//  Created by HongDT on 11/24/20.
//

import SwiftUI

struct DetailCityView: View {
    let detailWeather: CurrentWeather
    
    var body: some View {
        VStack {
            Text(detailWeather.cityName ?? "N/A")
                .font(.largeTitle)
                .fontWeight(.heavy)
            Text("Temp : \(String(format: "%.2f °C ", (detailWeather.main?.temp ?? 0) - 273.15))")
                .font(.subheadline)
            Text("Humidity : \(String(format: "%.2f °C ", detailWeather.main?.humidity ?? 0))")
                .font(.subheadline)
            Text("Description : \(detailWeather.weathers?[0].description ?? "N/A")")
                .font(.subheadline)
        }
    }
}

//struct DetailCityView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailCityView()
//    }
//}
