//
//  AddCity.swift
//  WeatherApp
//
//  Created by HongDT on 11/24/20.
//

import SwiftUI

struct AddCity: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var city = ""
    @State private var showingAlert = false
    private var contentError = ""
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("City Name")) {
                    TextField("Enter your city", text: $city)
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(
            leading: Button(action: {presentationMode.wrappedValue.dismiss()}, label: {
            Text("Cancel")
        }),
            trailing: Button(action: {addNewCity()}, label: {
            Text("Add")
            .alert(isPresented:$showingAlert) {
                Alert(title: Text("Error"), message: Text(AppData.messageError), dismissButton: .default(Text("OK")))
            }
        }))
    }
    
    func addNewCity() {
        let name = city.isEmpty ? "Hanoi" : city
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(name)&appid=\(AppData.apiKey)") else {
            print("Invalid URL")
            AppData.messageError = "Something went wrong."
            self.showingAlert = true
            return
        }
        
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                if let weather = try? JSONDecoder().decode(CurrentWeather.self, from: data) {
                    DispatchQueue.main.async {
                        if let cod = weather.cod, cod == 200, let id = weather.cityID {
                            if (!AppData.cityList.cities.contains(id)) {
                                AppData.cityList.addCity(city: id)
                                presentationMode.wrappedValue.dismiss()
                            } else {
                                AppData.messageError = "City have existed. Try another city."
                                self.showingAlert = true
                            }
                        } else {
                            AppData.messageError = "City not found. Please try again."
                            self.showingAlert = true
                        }
                    }
                    return
                }
            }
            print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            AppData.messageError = "Something went wrong."
            self.showingAlert = true
        }.resume()
    }
}

//struct AddCity_Previews: PreviewProvider {
//    static var previews: some View {
//        AddCity()
//    }
//}
