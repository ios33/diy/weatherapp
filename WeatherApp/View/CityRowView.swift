//
//  CityRowView.swift
//  WeatherApp
//
//  Created by HongDT on 11/24/20.
//

import SwiftUI

struct CityRowView: View {
    var currentWeather: CurrentWeather
    
    var body: some View {
        HStack {
            Text(currentWeather.cityName ?? "N/A").font(.headline)
            Spacer()
//            VStack {
//                Image(systemName: "cloud.sun.fill")
//                    .imageScale(.large)
//                    .foregroundColor(.yellow)
                Text(String(format: "%.2f °C ", (currentWeather.main?.temp ?? 0) - 273.15)) // convert Kelvin to celsius
                    .font(.subheadline)
//            }
        }
    }
}

//struct CityRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        CityRowView(city: City(name: "Hanoi"))
//    }
//}
