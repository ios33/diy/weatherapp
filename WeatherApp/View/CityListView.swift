//
//  CityListView.swift
//  WeatherApp
//
//  Created by HongDT on 11/24/20.
//

import SwiftUI
import SwiftUIRefresh

struct CityListView: View {
    @ObservedObject var currentWeatherViewModel: CurrentWeatherViewModel
    @State private var showingAlert = false
    @State private var currentCityID : Int64 = 0;
    @State private var isShowingLoadingPullToRefresh = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(currentWeatherViewModel.weathers, id: \.cityID) { weather in
                    NavigationLink(destination: DetailCityView(detailWeather: weather)) {
                        CityRowView(currentWeather: weather)
                            .onLongPressGesture {
                                print("Long press \(weather.cityName ?? "N/A")")
                                self.showingAlert = true
                                self.currentCityID = weather.cityID ?? 0
                            }
                            .alert(isPresented:$showingAlert) {
                                Alert(title: Text("Are you sure you want to delete this?"), message: Text("There is no undo"), primaryButton: .destructive(Text("Delete")) {
                                    if let index = AppData.cityList.cities.firstIndex(of: self.currentCityID) {
                                        AppData.cityList.deleteCity(at: IndexSet([index]))
                                        currentWeatherViewModel.fetchData(completionHandler: {})
                                    } 
                                }, secondaryButton: .cancel())
                            }
                    }
                }
            }
            .pullToRefresh(isShowing: $isShowingLoadingPullToRefresh) {
                currentWeatherViewModel.fetchData(completionHandler: {
                    self.isShowingLoadingPullToRefresh = false
                })
            }
            .navigationBarTitle(Text("Weather in your city"))
            .navigationBarItems(
                trailing:
                NavigationLink(destination: AddCity()) {
                  HStack {
                    Image(systemName: "plus")
                  }
                }
            )
            .onAppear() {
                currentWeatherViewModel.fetchData(completionHandler: {})
            }
        }
    }
}
//
//struct CityListView_Previews: PreviewProvider {
//    static var previews: some View {
//        CityListView(cityList: CityListData())
//    }
//}
